"""
This script reads numbers from an input file and counts how many
4-element windows in the list of numbers meet the condition
first_element < last_element.
"""
# pylint: disable=duplicate-code


def open_input_file(file_path):
    """
    Reads numbers from an input file.
    Returns: List of integers from the file.
    """
    try:
        with open(file_path, "r", encoding="utf-8") as file_handle:
            return [int(line.strip()) for line in file_handle.readlines()]
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return []


def count_four_elements_condition(numbers):
    """
    Counts how many 4-element windows meet the condition
    first_element < last_element.
    Returns: The count as an integer.
    """
    count = 0
    for i in range(len(numbers) - 3):
        first_element, _, _, last_element = numbers[i:i + 4]
        if first_element < last_element:
            count += 1
    return count


def main():
    """
    Main function that orchestrates the other functions.
    """
    input_file_path = "input.txt"
    numbers = open_input_file(input_file_path)

    if numbers:
        result = count_four_elements_condition(numbers)
        print(f"{result} windows meet the condition.")


if __name__ == "__main__":
    main()
