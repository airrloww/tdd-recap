"""
This script multiplies the final horizontal position by the final depth.
"""
# pylint: disable=duplicate-code


def open_input_file(file_path):
    """
    Reads: depth measurements from a file.
    Returns: A list of strings, each representing a line in the file.
    """
    try:
        with open(file_path, "r", encoding="utf-8") as file:
            return file.readlines()
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return []


def multiply(input_lines):
    """
    Processes a list of command lines and returns h * d.
    """
    aim = horizontal = depth = 0
    for line in input_lines:
        line = line.split()
        instruction, update = line[0], int(line[1])
        if instruction == "up":
            aim -= update
        elif instruction == "down":
            aim += update
        elif instruction == "forward":
            horizontal += update
            depth += aim * update
    return horizontal * depth


def main():
    """
    The main function that calls the functions.
    """
    file_path = "input.txt"
    lines = open_input_file(file_path)
    if lines:
        result = multiply(lines)
        print(result)


if __name__ == "__main__":
    main()
