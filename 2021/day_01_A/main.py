"""
This script reads depth measurements from an input file
and counts the measurements larger than their previous one.
"""
# pylint: disable=duplicate-code


def open_input_file(file_path):
    """
    Reads: depth measurements from a file.
    Returns: measurements as a list of integers.
    """
    try:
        with open(file_path, "r", encoding="utf-8") as file:
            return [int(line.strip()) for line in file.readlines()]
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return []


def count_increases(depth_list):
    """
    Counts:
    how many measurements in the list are larger than their previous.
    """
    count = 0
    for i in range(1, len(depth_list)):
        if depth_list[i] > depth_list[i - 1]:
            count += 1
    return count


if __name__ == "__main__":
    INPUT_FILE_PATH = "input.txt"
    depths = open_input_file(INPUT_FILE_PATH)

    if depths:
        RESULT = count_increases(depths)
        print(f"{RESULT} larger measurements.")
