"""
This script contains unit tests for the 'open_input_file'
and 'multiply' functions in the 'main' module.
"""
# pylint: disable=E0611 # pylint: disable=C0114
# pylint: disable=R0801 # pylint: disable=wrong-import-position
# pylint: disable=duplicate-code
import unittest
import os
from main import open_input_file, multiply  # noqa


class TestDayTwoPartOne(unittest.TestCase):
    """
    Test cases for the 'open_input_file' and 'multiply' functions
    for the first part.
    """
    def test_open_input_file(self):
        """
        Test the 'open_input_file' function
        """
        test_file = "test_input.txt"
        with open(test_file, "w", encoding="utf-8") as file:
            file.write("forward 10\nup 5\ndown 3")
        expected_lines = ["forward 10\n", "up 5\n", "down 3"]
        self.assertEqual(open_input_file(test_file), expected_lines)
        os.remove(test_file)

    def test_multiply(self):
        """
        Test the 'multiply' function.
        """
        input_lines = ["forward 10\n", "up 5\n", "down 3"]
        result = multiply(input_lines)
        self.assertEqual(result, -20)


if __name__ == '__main__':
    unittest.main()
